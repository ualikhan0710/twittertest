//
//  HomeDatasource.swift
//  Twitter
//
//  Created by Уали on 12/26/18.
//  Copyright © 2018 Ualikhan. All rights reserved.
//

import LBTAComponents
import TRON
import SwiftyJSON

class HomeDatasource: Datasource,JSONDecodable {
    
    let users: [User]
    
    required init(json: JSON) throws {
        //            print("Now ready to parse json: \n",json)
        
//        var users = [User]()
        
        let userJsonArray = json["users"].array
        
        self.users = userJsonArray!.map{User(json: $0)}
        
//        for userJson in userJsonArray!{
//            let user = User(json: userJson)
//            users.append(user)
//        }
        
//        var tweets = [Tweet]()
        let tweetsJsonArray = json["tweets"].array
        
        self.tweets = tweetsJsonArray!.map{Tweet(json: $0)}
        
//        for tweetsJson in tweetsJsonArray!{
//            let tweet = Tweet(json: tweetsJson)
//            tweets.append(tweet)
//        }
        
//            let userJson = tweetsJson["user"]
//            let user = User(json: userJson)
//
////            let name = userJson["name"].stringValue
////            let username = userJson["username"].stringValue
////            let bio = userJson["bio"].stringValue
////
////            let user = User(name: name,username: username,bioText: bio, profileImage: UIImage())
            
//            let message = tweetsJson["message"].stringValue
//            let tweet = Tweet(user: user, message: message)
            
            //}
        
//        self.users = users
//        self.tweets = tweets
    }
    
    
    let tweets: [Tweet]
        
    
    override func footerClasses() -> [DatasourceCell.Type]? {
        return [UserFooter.self]
    }
    
    
    override func headerClasses() -> [DatasourceCell.Type]? {
        return [UserHeader.self]
    }
    
    
    override func cellClasses() -> [DatasourceCell.Type] {
        return [UserCell.self, TweetCell.self]
    }
    
    override func item(_ indexPath: IndexPath) -> Any? {
        if indexPath.section == 1{
            return tweets[indexPath.item]
        }
        return users[indexPath.item]
    }
    
    override func numberOfSections() -> Int {
        return 2
    }
    
    override func numberOfItems(_ section: Int) -> Int {
        if section == 1 {
            return tweets.count
        }
        return users.count
    }
    
}

//    let users: [User] = {
//        let ualiUser = User(name: "Ualikhan", username: "@sabdenov_",bioText: "iPhone,IPad,IOS programming community.Join us to learn swift,Objective-C and build apps", profileImage: UIImage(named: "profile_image")!)
//
//        let asekUser = User(name: "Assylbek", username: "@talgatuly", bioText: "Ios iPhone.Join us to learn swift,Objective-C and build apps", profileImage: UIImage(named: "ray_profile_image")!)
//
//        let dikoUser = User(name: "Didar", username: "@tazhiman_d", bioText: "Web developer Lbflda ascass assdad asdasd asds das asd a dsad asdas das das as ds dadsadas sada sda sda dasadas asdasdsa dsadasd as dadasd asdas ads sadasdas djsjfds asajfsfj dfdsf fwef qe ffsd  sd sdf sfsd fd sf sdf sdfsd gf g g hdfdgsdfa dfa fsgsfd dfa ascsdv dacdsvs", profileImage: UIImage(named: "profile_image")!)
//
//        return [ualiUser,asekUser,dikoUser]
//    }()

//let tweets: [Tweet] = {
//    let ualiUser = User(name: "Ualikhan", username: "@sabdenov_",bioText: "iPhone,IPad,IOS programming community.Join us to learn swift,Objective-C and build apps", profileImage: UIImage(named: "profile_image")!)
//    let tweet = Tweet(user: ualiUser, message: "1asdas ads sadasdas djsjfds asajfsfj dfdsf fwef qe ffsd  sd sdf sfsd fd sf sdf sdfsd gf g g hdfdgsdfa dfa fsgsfd dfa ascsdv dacdsv iPhone,IPad,IOS programming community.Join us to learn swift,Objective-C and build apps")
//
//    let tweet2 = Tweet(user: ualiUser, message: "this is a 2nd asdas ads sadasdas djsjfds asajfsfj dfdsf fwef qe ffsd  sd sdf sfsd fd sf sdf sdfsd gf g g hdfdgsdfa dfa fsgsfd dfa ascsdv dacdsv iPhone,IPad,IOS programming community.Join us to learn swift,Objective-C and build apps")
//    return [tweet,tweet2]
//}()
