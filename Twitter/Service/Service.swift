//
//  Service.swift
//  Twitter
//
//  Created by Уали on 1/9/19.
//  Copyright © 2019 Ualikhan. All rights reserved.
//

import Foundation
import TRON
import SwiftyJSON

struct Service {
    
    let tron = TRON(baseURL: "https://api.letsbuildthatapp.com")
    
    static let sharedInstance  = Service()
    
    
    
    func fetchHomeFeed(completion: @escaping (HomeDatasource) -> ()){
        print("Fetching home feed")
        
        //start our json fetch
        
        let request: APIRequest<HomeDatasource, JSONError> = tron.swiftyJSON.request("/twitter/home")
        
        
        request.perform(withSuccess: { (homeDatasource) in
            print("Successfully fetched our json objects")
            
//            print(homeDatasource.users.count)
            
            completion(homeDatasource)
            
//            self.datasource = homeDatasource
            
        }) { (err) in
            print("Failed to fecth json...",err)
        }
        //        request.method = .
        
        //use tron
        //        URLSession.shared.dataTask(with: <#T##URL#>, completionHandler: <#T##(Data?, URLResponse?, Error?) -> Void#>)
        
        print(2)
    }
    
    class JSONError: JSONDecodable {
        required init(json: JSON) throws {
            print("JSON ERROR")
        }
    }
    
}
